FROM python:3.9.13-slim AS compile-image
LABEL maintainer="sendurarda@gmail.com"

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential gcc 

WORKDIR /
COPY src/requirements.txt .
RUN pip install --user --no-cache-dir -r requirements.txt

#Builder pattern
FROM python:3.9.13-slim  AS build-image
COPY --from=compile-image /root/.local /root/.local
WORKDIR /
COPY ./src/app.py app.py

EXPOSE 80
# Make sure scripts in .local are usable:
ENV PATH=/root/.local/bin:$PATH
CMD ["python" ,"app.py"]



