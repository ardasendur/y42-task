
from flask import Flask,jsonify
import os
from pathlib import Path

app = Flask(__name__)
@app.route('/')
def start():
    build_time=get_date_from_file('output/build-time')
    deploy_time=get_date_from_file('output/deploy-time')
    return jsonify(
        {
        "hello": "world", 
        "built_at": build_time,
        "deployed_at": deploy_time
        }
    )
def get_date_from_file(path_of_file):
    date = Path(path_of_file).read_text()
    date = date.replace('\n', '')
    return date
# main driver function
if __name__ == '__main__':
  
    # run() method of Flask class runs the application 
    # on the local development server.
   port = int(os.environ.get('PORT', 5000))
   app.run(debug=True, host='0.0.0.0', port=port)