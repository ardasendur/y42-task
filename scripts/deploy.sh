#!/bin/bash

#######################################
# DESCRIPTION: This script generates helm release which contains service,deployment,pod and replica set k8s objects and one deploy time stamp file in output folder .
# MAINTAINER: Arda Sendur
# USAGE: bash deploy.sh  or ./deploy.sh
# DATE: 21.05.2022
#######################################


set -eo pipefail
set -o errexit

#Constants
readonly HELM_CHART_PATH="../deployment/app"
readonly NAMESPACE="app"
readonly DEPLOY_TIME_STAMP_FILE_NAME="output/deploy-time"
readonly APP_NAME="app" 

#######################################
# Deploy helm release with app namespace
# Globals:
#   NAMESPACE
#   HELM_CHART_PATH
# Arguments:
#   None
#######################################
deploy(){
        printf "Deploy the image with helm.\n"
        helm upgrade --install --create-namespace $NAMESPACE -n $NAMESPACE --debug --atomic $HELM_CHART_PATH
}

#######################################
# Generate one deployed time-stamp file
#   DEPLOY_TIME_STAMP_FILE_NAME
# Arguments:
#   None
#######################################

get_deployed_time(){

    kubectl get deploy $APP_NAME -o jsonpath='{.metadata.creationTimestamp}' > $DEPLOY_TIME_STAMP_FILE_NAME
    printf "Time stamp was added in file %s.\n" "$DEPLOY_TIME_STAMP_FILE_NAME"

}
#######################################
# Check helm release status and k8s objects
# Globals:
#   NAMESPACE
#   APP_NAME
# Arguments:
#   None
#######################################
check_helm_release_status(){
    helm status $APP_NAME -n $NAMESPACE
    kubectl get all -n $NAMESPACE  
}
main(){
    deploy
    get_deployed_time
    check_helm_release_status
}
main "$@"
