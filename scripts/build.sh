#!/bin/bash
#######################################
# DESCRIPTION: This script generates one build time stamp file in output folder and docker image.
# MAINTAINER: Arda Sendur
# USAGE: bash build.sh or ./build.sh
# DATE: 21.05.2022
#######################################

set -eo pipefail
set -o errexit

### Constants ###
readonly REPO="task-y42"
readonly BUILD_TIME_STAMP_FILE_NAME="output/build-time"
readonly REGISTRY_NAME="ardasendur/app:latest"

#######################################
# Build Docker image from DockerFile
# Globals:
#   REPO
# Arguments:
#   None
#######################################

build_image(){
    printf "Buiding docker image is starting."
    # Navigate Dockerfile path 
    cd ..
    docker build --no-cache -t ${REPO} .
    cd -
    
}
#######################################
# Get docker image id from tag name 
# Globals:
#   REPO
# Arguments:
#   None
#######################################

get_image_id(){
    printf "Get IMAGE_ID method is running."
    IMAGE_ID=$(docker images | grep -E "^$REPO.*latest" | awk '{print $3}')
    printf "IMAGE_ID of Docker Images: %s\n" "$IMAGE_ID"
}
#######################################
# Generate one build time-stamp file 
# Globals:
#   BUILD_TIME_STAMP_FILE_NAME
# Arguments:
#   IMAGE_ID
#######################################

get_image_time_stamp(){
    printf "Get image time stamp is starting.\n"
    docker inspect -f '{{ .Created }}' "$1" > $BUILD_TIME_STAMP_FILE_NAME
    printf "Time stamp was added in file %s.\n" "$BUILD_TIME_STAMP_FILE_NAME"
}

#######################################
# Tagged and pushed docker image
# Globals:
# Arguments:
#   IMAGE_ID
#   REGISTRY_NAME
#######################################
push_docker_image_public_registry(){
    printf "Tag and pushed image.\n"
    docker tag "$1" "$2"
    docker push "$2"
    printf "Image pushed : %s\n" "$2"
}

### Main ###
main(){
    build_image
    get_image_id
    get_image_time_stamp "${IMAGE_ID}"
    push_docker_image_public_registry "${IMAGE_ID}" "${REGISTRY_NAME}"
}

main "$@" 
