# Y42-task

```
cd Y42-TASK
git remote add origin https://gitlab.com/ardasendur/y42-task.git
git branch -M main
git push -uf origin main
```
## Test and Deploy
    To create and deploy please follow below commands for minikube :
    ```
    cd scripts
    bash build.sh
    bash deploy.sh
    cd output ; ls -alrth
    cd ..
    export NODE_PORT=$(kubectl get --namespace app -o jsonpath="{.spec.ports[0].nodePort}" services app)
    export NODE_IP=$(kubectl get nodes --namespace app -o jsonpath="{.items[0].status.addresses[0].address}")
    echo http://$NODE_IP:$NODE_PORT
    curl -kl http://$NODE_IP:$NODE_PORT
    ```
    To run docker container and test :
    ```
    docker run -idt  -v $(pwd)/output:/output -p 80:5000 <IMAGE_ID> || <IMAGE_NAME> 
    curl -kl localhost:80
    ``` 
***

## Name
    Y42 Devops Task
## Description
This project creates one docker image and deploys this image local kubernetes cluster.Moreover, docker container exposes with 80 port and it shows deployed and build time stamp.
'scripts' folder contains two bash scripts. 'build.sh' is responsible for building docker image and creating build-time file inside output folder.
'deploy.sh' is responsible for deploying application in minikube cluster with helm and creating deploy-time file inside output folder.This output folder is using
in docker volume. Also,pod is using hostPath volume.
To deploy application in minikube cluster, I used helm package manager.'deployment/app' folder contains helm charts.Custom values are setting inside 'values.yaml'.These values are located in
image,service and volume part. K8S deployment includes 'hostPath' type volume to get last updated deployed and build time stamp.
Gitlab pipeline has one lint stage for shell and docker file linter.


## CI/CD
These project includes 1 stage which are described below:

lint : Docker file check and lint by using Hadolint and bash script checker

## TECH
    This repository is using different technology work properly:

    ✨ Docker
    ✨ Kubernetes
    ✨ Bash  Scripting
    ✨ Python
    ✨ Helm

## Visuals
You can also get two demo videos inside screen-shot&videos folder.

![K8S Resources](screen-shots&video/k8s-resources.png)
![App Curl Output](screen-shots&video/curl-output-minikube.png)
![Docker Container](screen-shots&video/docker-container.png)
![Docker Curl Output](screen-shots&video/curl-container.png)

